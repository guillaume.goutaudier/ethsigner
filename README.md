# Introduction

In this tutorial we want to demonstrate how to sign Ethereum transactions using a Hardware Security Module (HSM). 
HSM devices are capable or creating and storing encryption keys securely. They can also perform cryptographic operations directly at the hardware level, i.e. without the need to ever expose the encryption keys. There features can be used to store Ethereum (or Bitcoin) provide keys and sign transactions. 

All major Cloud providers now offer some form of HSM solution, though not all of them support the type of encryption used by Ethereum.

The way Ethereum transactions are crafted is not trivial, so we will use EthSigner to deletage the transaction signing process:
![architecture.png](architecture/architecture.png)  

Most of the steps in this tutorial follow the EthSigner official documentation:
https://docs.ethsigner.consensys.net/
 

# Prerequisites
- A running Ethereum 1.0 node: we used Geth on the Rinkeby test network it should work will most Ethereum clients and networks
- Docker
- An Azure account

# Creation of the key on the HSM Service
EthSigner currently supports Azure Key Vault and Hashicorp Vault as external key stores. 
We will use Azure Key Vault which seems to be the most secure solution, as it allows for the signing of the transactions directly by the HSM service (i.e. the private keys never leave the HSM device).

On your Azure account, create a new Key Vault and generate a new Key with the following parameters:
- Key type: `EC-HSM`
- Elliptic curve name: `P-256K`

You also need to grand acces to the Key Vault to EthSigner. You can do this by registering a new application in the "App Registrations" menu. Once this is done, you need to create a new client secret that will be used by the application to actually connect to the Cloud service:
![secret.png](architecture/secret.png)  

Collect all the following information for the next steps:
- Key vault name (as displayed in the interface)
- Key name (as displayed in the interface)
- Key version, e.g. "b2696562de464bac89eedbacff1cc587"
- Client ID, e.g. "25a2ec8b-fa2e-47cb-b012-4742c5dac901"
- Secret
- Tenant ID, e.g. "309ca3b9-xxxx-xxxx-a66a-xxxxxxxx3e85"


# EthSigner setup
- Record the secret in a local file e.g. `secretFile`
- Start EthSigner from the directory where the secret file is stored (warning: many of the start-up options are insecure):
```
docker run --name ethsigner -p 8545:8545 --mount type=bind,source=$PWD,target=/var/lib/ethsigner pegasyseng/ethsigner:latest --chain-id=4 --downstream-http-host=<IP of your Eth node> --downstream-http-port=<HTTP port of your Eth node> --http-listen-host=0.0.0.0 azure-signer --client-id=<Client ID previously recorded> --client-secret-path=/var/lib/ethsigner/secretFile --key-name=<Key name previously recorded> --key-version=<Key version previously recorded> --keyvault-name=<Vault name previously recorded> --tenant-id=<Tenant ID previously recorded>
```
- A first thing that can be done to validate EthSigner properly interacts with Geth is to check the blockNumber:
```
curl -X POST -H "Content-Type: application/json" --data '{"jsonrpc":"2.0","method":"eth_blockNumber","params":[],"id":51}' http://127.0.0.1:8545
```

# Sign and submit transactions
- First we need to retrieve the address that corresponds to the key that has been generated on the HSM device:
```
curl -X POST -H "Content-Type: application/json" --data '{"jsonrpc":"2.0","method":"eth_accounts","params":[],"id":1}' http://127.0.0.1:8545
```
- Now need to send some (fake) ETH to this address (with Metamask for example)
- Now we can request EthSigner to sign and send a transaction, e.g.: 
```
curl -X POST -H "Content-Type: application/json" --data '{"jsonrpc":"2.0","method":"eth_sendTransaction","params":[{"from": <Address from previous step>,"to": <Whatever address you want>,"gas": "0x7600","gasPrice": "0x3B9ACA00","value": "0x123", "data": "0x54455354"}]}' http://127.0.0.1:8545
```
- We should see the signing request in the docker logs, e.g.:
```
2021-06-11 08:54:54.379+00:00 | vert.x-worker-thread-3 | INFO  | CryptographyServiceClient | Signing content with algorithm - ES256K
2021-06-11 08:54:57.030+00:00 | reactor-http-epoll-2 | INFO  | CryptographyServiceClient | Retrieved signed content with algorithm- ES256K
```
- Once mined, the transaction will also be visible on Etherscan:
![etherscan.png](architecture/etherscan.png)  


# Stepping back
Really cool, right? What bothers me though is the fact that we still need to store the secret to access the Cloud Vault somewhere. So in terms of security, if this secret gets compromised the consequence will be the same as if we lost the private key (as knowing this secret allows to steal al the funds). Managing this secret securely will therefore be key for any production implementation.  




